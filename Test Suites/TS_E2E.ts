<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_E2E</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6d691563-eec6-4006-91c4-f0592bf59486</testSuiteGuid>
   <testCaseLink>
      <guid>08f4c450-a0f8-4965-889b-10187b7b708d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01 Register</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa5587f7-7c5c-4f8a-b85f-23b9e99bd74d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2613b59-dc8e-47f9-9033-f861038a3af4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03 Account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1c04b89-2fed-407d-9089-3706053635e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04 Make Deposit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36badc77-d6f7-4e81-8843-51f6c9e4fa4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/05 Transfer</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
