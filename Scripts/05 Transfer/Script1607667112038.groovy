import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Object Repository/burger menu'), 0)

Mobile.tap(findTestObject('menu_trf/menu_transfer'), 0)

//Mobile.tap(findTestObject('menu_trf/dropdown_pengirim'), 0)

//Mobile.tap(findTestObject('menu_trf/pilihan_2 pengirim'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('menu_trf/field_Amount'), '100', 0)

//Mobile.tap(findTestObject('menu_trf/dropdown_penerima'), 0)

//Mobile.tap(findTestObject('menu_trf/pilihan_3 penerima'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('menu_trf/btn_transfer'), 0)

